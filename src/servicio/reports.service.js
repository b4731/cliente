import axios from "axios";
let token = localStorage.getItem("tokenAuth");

let reporteService = {
    async pedidos(fechaIni,fechaFin) {
       return axios({
           url: "/reportes/pedidos?fechaIni=" + fechaIni + "&fechaFin=" + fechaFin,
           method: "GET",

       });

    },
    async pedidos_diarios(fechaIni,fechaFin) {
        return axios({
            url: "/reportes/pedidos_diarios?fechaIni=" + fechaIni + "&fechaFin=" + fechaFin,
            method: "GET",

        });
    },

    async resumen_pedidos_diarios(fechaIni,fechaFin) {
        return axios({
            url: "/reportes/resumen/pedidos_diarios?fechaIni=" + fechaIni + "&fechaFin=" + fechaFin,
            method: "GET",

        });
    },
    async ventas(fechaIni,fechaFin) {
        return axios({
            url: "/reportes/ventas?fechaIni=" + fechaIni + "&fechaFin=" + fechaFin,
            method: "GET",

        });

    },
    async ventas_diarios(fechaIni,fechaFin) {
        return axios({
            url: "/reportes/ventas_diarios?fechaIni=" + fechaIni + "&fechaFin=" + fechaFin,
            method: "GET",

        });
    },

    async resumen_ventas_diarios(fechaIni,fechaFin) {
        return axios({
            url: "/reportes/resumen/ventas_diarios?fechaIni=" + fechaIni + "&fechaFin=" + fechaFin,
            method: "GET",

        });
    }
};

export default reporteService;
