console.log("navegacion");
let usuario = localStorage.getItem("usuario");
let rutas = [];
if (usuario !== null) {
  let usuarioParse = JSON.parse(usuario);
  console.log(usuarioParse);
  let rol = usuarioParse.rol;
  console.log(rol);

  if (rol == 2) {
    rutas = [
      {
        _name: "CSidebarNav",
        _children: [
          {
            _name: "CSidebarNavItem",
            name: "Dashboard",
            to: "/dashboard",
            icon: "cil-speedometer",
            badge: {
              color: "primary",
              text: "NEW",
            },
          },
          {
            _name: "CSidebarNavTitle",
            _children: ["Components"],
          },

          {
            _name: "CSidebarNavDropdown",
            name: "Reservaciones",
            route: "/buttons",
            icon: "cil-cursor",
            items: [
              {
                name: "Catalogo",
                to: "/reservacion/catalogo",
              },

              {
                name: "Reservacion",
                to: "/reservacion/reservacion",
              },
            ],
          },

          {
            _name: "CSidebarNavDivider",
            _class: "m-2",
          },
        ],
      },
    ];
  }

  if (rol == 3) {
    rutas = [
      {
        _name: "CSidebarNav",
        _children: [
          {
            _name: "CSidebarNavItem",
            name: "Dashboard",
            to: "/dashboard",
            icon: "cil-speedometer",
            badge: {
              color: "primary",
              text: "NEW",
            },
          },
          {
            _name: "CSidebarNavTitle",
            _children: ["Components"],
          },

          {
            _name: "CSidebarNavDropdown",
            name: "citas",
            route: "/empleados",
            icon: "cil-cursor",
            items: [
              {
                name: "Citas",
                to: "/empleados/pendientes",
              },
            ],
          },
        ],
      },
    ];
  }

  if (rol == 1) {
    rutas = [
      {
        _name: "CSidebarNav",
        _children: [
          {
            _name: "CSidebarNavItem",
            name: "Dashboard",
            to: "/dashboard",
            icon: "cil-speedometer",
            badge: {
              color: "primary",
              text: "NEW",
            },
          },
          {
            _name: "CSidebarNavTitle",
            _children: ["Acceso"],
          },
          {
            _name: "CSidebarNavDropdown",
            name: "Acceso",
            route: "/acceso",
            icon: "cil-puzzle",
            items: [
              {
                name: "Usuario",
                to: "/acceso/usuario",
              },
            ],
          },
          {
            _name: "CSidebarNavTitle",
            _children: ["Components"],
          },
          {
            _name: "CSidebarNavDropdown",
            name: "Negocio",
            route: "/negocio",
            icon: "cil-puzzle",
            items: [
              {
                name: "Empresas",
                to: "/negocio/empresas",
              },
              {
                name: "Propietarios",
                to: "/negocio/propietarios",
              },
              {
                name: "Clientes",
                to: "/negocio/clientes",
              },
              {
                name: "Empleados",
                to: "/negocio/empleados",
              },
            ],
          },
          {
            _name: "CSidebarNavDropdown",
            name: "Reservaciones",
            route: "/reservacion",
            icon: "cil-cursor",
            items: [
              {
                name: "Citas",
                to: "/reservacion/cita",
              },
            ],
          },
          {
            _name: "CSidebarNavDropdown",
            name: "Maestros",
            route: "/maestros",
            icon: "cil-cursor",
            items: [
              {
                name: "Servicios",
                to: "/maestros/servicios",
              },
              {
                name: "Catalogo",
                to: "/maestros/catalogos",
              },
            ],
          },
          {
            _name: "CSidebarNavItem",
            name: "Manual",
            to: "/manual",
            icon: "cil-chart-pie",
          },
          {
            _name: "CSidebarNavDropdown",
            name: "Reportes",
            route: "/reportes",
            icon: "cil-cursor",
            items: [
              {
                name: "Ventas",
                to: "/reportes/ventas",
              },
              {
                name: "Pedidos",
                to: "/reportes/Pedidos",
              },
            ],
          },
          {
            _name: "CSidebarNavDivider",
            _class: "m-2",
          },
        ],
      },
    ];
  }
}

export default rutas;
