const countersModule = {
    state: {
        counterReservacion: 0,
        finalizarContadorReservacion: false,
        finalizar: false,
        counterPago: 0,
        finalizarContadorPago: false,
        finalizarPago: false,
        counterReporteVenta: 0,
        finalizarContadorReporteVenta: false,
        finalizarReporteVenta: false,
        counterReportePedido: 0,
        finalizarContadorReportePedido: false,
        finalizarReportePedido: false
    },
    mutations: {
        reiniciar(state) {
            state.counterReservacion = 0
            state.finalizarContadorReservacion = false
            state.finalizar = false
            state.counterPago = 0
            state.finalizarContadorPago = false
            state.finalizarPago = false
            state.counterReporteVenta = 0
            state.finalizarContadorReporteVenta = false
            state.finalizarReporteVenta = false
            state.counterReportePedido = 0
            state.finalizarContadorReportePedido = false
            state.finalizarReportePedido = false
        },
        defaultCounterReservacion(state) {
            state.counterReservacion = 0
        },
        setCounterReservacion(state) {
            state.counterReservacion += 1
        },
        initReservacion(state) {
            state.finalizarContadorReservacion = true
        },
        finReservacion(state) {
            state.finalizarContadorReservacion = false
            state.finalizar = true
            state.counterReservacion = 0
        },
        defaultCounterPago(state) {
            state.counterPago = 0
        },
        setCounterPago(state) {
            state.counterPago += 1
        },
        initPago(state) {
            state.finalizarContadorPago = true
        },
        finPago(state) {
            state.finalizarContadorPago = false
            state.finalizarPago = true
            state.counterPago = 0
        },
        defaultCounterReporteVenta(state) {
            state.finalizarContadorReporteVenta=false
            state.finalizarReporteVenta=false
            state.counterReporteVenta = 0
        },
        setCounterReporteVenta(state) {
            state.counterReporteVenta += 1
        },
        initReporteVenta(state) {

            state.finalizarContadorReporteVenta = true
        },
        finReporteVenta(state) {
            state.finalizarContadorReporteVenta= false
            state.finalizarReporteVenta = true
            // state.counterReporteVenta = 0
        },
        defaultCounterReportePedido(state) {
            state.finalizarContadorReportePedido=false
            state.finalizarReportePedido=false
            state.counterReportePedido= 0
        },
        setCounterReportePedido(state) {
            state.counterReportePedido += 1
        },
        initReportePedido(state) {
            state.finalizarContadorReportePedido= true
        },
        finReportePedido(state) {
            state.finalizarContadorReportePedido= false
            state.finalizarReportePedido = true
            // state.counterReportePedido = 0
        },
    },
    actions: {
        ReiniciarStados(context) {
            context.commit('reiniciar')
        },
        incrementCounterReservacion(context) {
            context.commit('setCounterReservacion')
            context.commit('defaultCounterReservacion')
        },
        finContadorReservacion(context) {
            context.commit('finReservacion')
        },
        initContadorReservacion(context) {
            console.log('init contador')
            context.commit('initReservacion')
            context.commit('setCounterReservacion')
            if (context.state.finalizarContadorReservacion) {
                setTimeout(function () {
                    if (!context.state.finalizar) {
                        context.dispatch('initContadorReservacion')
                    }
                }, 1000)
            }

        },
        incrementCounterPago(context) {
            context.commit('setCounterPago')
            context.commit('defaultCounterPago')
        },
        finContadorPago(context) {
            context.commit('finPago')
        },
        initContadorPago(context) {
            console.log('init pago')
            context.commit('initPago')
            context.commit('setCounterPago')
            if (context.state.finalizarContadorPago) {
                setTimeout(function () {
                    if (!context.state.finalizarPago) {
                        context.dispatch('initContadorPago')
                    }
                }, 1000)
            }

        },
        incrementCounterReporteVenta(context) {
            context.commit('setCounterReporteVenta')
            context.commit('defaultCounterReporteVenta')
        },
        finContadorReporteVenta(context) {
            context.commit('finReporteVenta')
        },
        initContadorReporteVenta(context) {
            console.log('init pago')
            context.commit('initReporteVenta')
            context.commit('setCounterReporteVenta')
            if (context.state.finalizarContadorReporteVenta) {
                setTimeout(function () {
                    if (!context.state.finalizarReporteVenta) {
                        context.dispatch('initContadorReporteVenta')
                    }
                }, 1000)
            }

        },
        incrementCounterReportePedido(context) {
            context.commit('setCounterReportePedido')
            context.commit('defaultCounterReportePedido')
        },
        finContadorReportePedido(context) {
            context.commit('finReportePedido')
        },
        initContadorReportePedido(context) {
            console.log('init pago')
            context.commit('initReportePedido')
            context.commit('setCounterReportePedido')
            if (context.state.finalizarContadorReportePedido) {
                setTimeout(function () {
                    if (!context.state.finalizarReportePedido) {
                        context.dispatch('initContadorReportePedido')
                    }
                }, 1000)
            }

        }
    },
    getters: {},
};

export default countersModule;
